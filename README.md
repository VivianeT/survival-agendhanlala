# Survival Agendhanlala

## What is it ?

It is a virtual agenda for those who are in confinement. 

Depending on the number of people in the house, and possessions, activities will be added to the day.

This project is using Symfony 4.4 !

## Requirements

Install Composer and Npm.

## Licence

This project is under Proprietary license. This means you can't use this project without asking [VivianeT](https://gitlab.com/VivianeT) or [M.Marchand](https://gitlab.com/M.Marchand).