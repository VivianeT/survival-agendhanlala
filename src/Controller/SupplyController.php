<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\SupplieSpace;


class SupplyController extends AbstractController
{
    /**
     * @Route("/supply", name="supply")
     */
    public function supply()
    {
        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        $username = $user->getUsername();
        $userId = $user->getId();

        $supplyRepository = $this->getDoctrine()->getRepository(SupplieSpace::class);

        $supplyArray =  $supplyRepository->findBy(['user' => $userId]);

        return $this->render('supply/supply.html.twig', [
            'controller_name' => 'SupplyController',
            'supplyArray' => $supplyArray,
            'username' => $username,
        ]);
    }
}
