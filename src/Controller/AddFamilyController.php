<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\FamilyMember;
use App\Entity\User;
use App\Form\AddFamilyType;

class AddFamilyController extends AbstractController
{
    /**
     * @Route("/addFamily", name="addFamily")
     */
    public function addFamily(Request $request)
    {
        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }

        $user= $this->getUser();
        $userId= $user->getId();
        $username =$user->getUsername();
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $currentUser =  $userRepository->findOneBy(['id' => $userId]);


        $newMember = New FamilyMember();
        $addMemberForm = $this->createForm(AddFamilyType::class, $newMember);
        $addMemberForm->handleRequest($request);

        if($addMemberForm->isSubmitted() && $addMemberForm->isValid()){
            $newMember->setUser($currentUser);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newMember);
            $entityManager->flush();
            return $this->redirect('family');

        }

        return $this->render('family/addFamily.html.twig', [
            'controller_name' => 'AddFamilyController',
            'addMemberForm' => $addMemberForm->createView(),
            "username"=>$username,
        ]);
    }
}
