<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ActivityRepository;
use App\Entity\Activity;
use Doctrine\ORM\EntityManager;

class ActivityController extends AbstractController
{
    private $activityRepository;
    private $entityManager;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository=$activityRepository;
   
    }
    /**
     * @Route("/activity", name="activity")
     */
    public function index(ActivityRepository $activityRepository)
    {
        
        
        $activityArray = $activityRepository->findAll();


        return $this->render('activity/index.html.twig', [
            'controller_name' => 'ActivityController',
            'activity'=>$activityArray,
         
        ]);
    }
}
