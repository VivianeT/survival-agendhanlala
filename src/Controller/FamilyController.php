<?php

namespace App\Controller;

use App\Entity\FamilyMember;
use App\Repository\FamilyMemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FamilyController extends AbstractController
{
    /**
     * @Route("/family", name="family")
     */
    public function family(FamilyMemberRepository $familyRepository)
    {
        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        $username = $user->getUsername();
        $userId = $user->getId();

        $familyRepository = $this->getDoctrine()->getRepository(FamilyMember::class);

        $familyArray =  $familyRepository->findBy(['user' => $userId]);

        return $this->render('family/family.html.twig', [
            'controller_name' => 'FamilyController',
            'familyArray' => $familyArray,
            'username' => $username,
        ]);
    }
}
