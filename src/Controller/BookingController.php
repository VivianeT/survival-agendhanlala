<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Activity;
use App\Repository\ActivityRepository;


/**
 * @Route("/booking")
 */
class BookingController extends AbstractController
{
    private $activityRepository;
    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository=$activityRepository;
        
    }
   /**
     * @Route("/agenda", name="agenda")
     */
    public function agenda(BookingRepository $bookingRepository)
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        return $this->render('booking/index.html.twig', [
            'controller_name' => 'BookingController',
            'bookings' => $bookingRepository->findAll(),
            'username' => $username,
        ]);
    }
     /**
     * @Route("/calendar", name="booking_calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $userColor = $user->getColor();
        return $this->render('booking/calendar.html.twig',[
            'username' => $username,
            'userColor' => $userColor,
        ]);
    }

    /**
     * @Route("/", name="booking_index", methods={"GET"})
     */
    public function index(BookingRepository $bookingRepository): Response
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        return $this->render('booking/index.html.twig', [
            'bookings' => $bookingRepository->findAll(),
            'username' => $username,
        ]);
    }

    /**
     * @Route("/new", name="booking_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($booking);
            $entityManager->flush();

            return $this->redirectToRoute('booking_index');
        }

        return $this->render('booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
            'username' => $username,
        ]);
    }

    /**
     * @Route("/{id}", name="booking_show", methods={"GET"})
     */
    public function show(Booking $booking): Response
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $userColor= $user->getColor();
        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'username' => $username,
            'userColor' => $userColor,

        ]);
    }

    /**
     * @Route("/{id}/edit", name="booking_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Booking $booking): Response
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('booking_index');
        }

        return $this->render('booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
            'username' => $username,
        ]);
    }

    /**
     * @Route("/{id}", name="booking_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Booking $booking): Response
    {
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute('booking_index');
    }
}
