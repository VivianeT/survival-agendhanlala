<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\UsernameType;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request)
    {
        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
        $email = $user->getEmail();

        $usernameForm = $this->createForm(UsernameType::class, $user);
        $usernameForm->handleRequest($request);
        if ($usernameForm->isSubmitted() && $usernameForm->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('profile');
        }

        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);
        $changePasswordForm->handleRequest($request);
        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()){
            $passwordEncoder = $this->get('security.password_encoder');
            $oldPassword = $request->request->get('change_password')['oldPassword'];

        // Si l'ancien mot de passe est bon
        if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
            $newEncodedPassword = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($newEncodedPassword);
            
            $entityManager->persist($user);
            $entityManager->flush();
            
            $this->redirect('profile');
            $this->addFlash('notice', 'Votre mot de passe à bien été changé !');
        }
    }
        return $this->render('agenda/profile.html.twig', [
            'controller_name' => 'ProfileController',
            'username' => $username,
            'email' => $email,
            'usernameForm' => $usernameForm->createView(),
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);

    }
}