<?php

namespace App\Controller;

use App\Entity\SupplieSpace;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\AddSupplyType;
use Symfony\Component\HttpFoundation\Request;

class AddSupplyController extends AbstractController
{
    /**
     * @Route("/addSupply", name="addSupply")
     */
    public function addSupply(Request $request)
    {
        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }

        $user= $this->getUser();
        $userId= $user->getId();
        $username= $user->getUsername();
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $currentUser =  $userRepository->findOneBy(['id' => $userId]);


        $newSupply = New SupplieSpace();
        $addSupplyForm = $this->createForm(AddSupplyType::class, $newSupply);
        $addSupplyForm->handleRequest($request);

        if($addSupplyForm->isSubmitted() && $addSupplyForm->isValid()){
            $newSupply->setUser($currentUser);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newSupply);
            $entityManager->flush();
            return $this->redirect('supply');
        }


        return $this->render('supply/addSupply.html.twig', [
            'controller_name' => 'AddSupplyController',
            "addSupplyForm" => $addSupplyForm->createView(),
            "username" => $username,
        ]);
    }
}
