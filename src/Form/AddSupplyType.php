<?php

namespace App\Form;

use App\Entity\SupplieSpace;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AddSupplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('Name', TextType::class, [
            'required' => true,
            'label' => 'Nom'
        ])
        ->add('Quantity', NumberType::class, [
            'required' => true,
            'label' => 'Quantité'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplieSpace::class,
        ]);
    }
}
