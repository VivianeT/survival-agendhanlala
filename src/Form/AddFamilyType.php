<?php

namespace App\Form;

use App\Entity\FamilyMember;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AddFamilyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, [
                'required' => true,
                'label' => 'Nom'
            ])
            ->add('Age', NumberType::class, [
                'required' => true,
                'label' => 'Âge'
            ])
            ->add('Color', ColorType::class, [
                'required' => true,
                'label' => 'Couleur'
            ])
            ->add('Type', ChoiceType::class, [
                'required' => true,
                'label' => 'Type',
                'choices'  => [
                    'Humain' => true,
                    'Animal' => false,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FamilyMember::class,
        ]);
    }
}
