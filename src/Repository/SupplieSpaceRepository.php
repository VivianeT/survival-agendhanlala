<?php

namespace App\Repository;

use App\Entity\SupplieSpace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SupplieSpace|null find($id, $lockMode = null, $lockVersion = null)
 * @method SupplieSpace|null findOneBy(array $criteria, array $orderBy = null)
 * @method SupplieSpace[]    findAll()
 * @method SupplieSpace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SupplieSpaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplieSpace::class);
    }

    // /**
    //  * @return SupplieSpace[] Returns an array of SupplieSpace objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SupplieSpace
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
