<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200414094527 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplie_space (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, quantity INT NOT NULL, INDEX IDX_23D5120A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplie_space_activity (supplie_space_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_1DB55858A5C4A3D6 (supplie_space_id), INDEX IDX_1DB5585881C06096 (activity_id), PRIMARY KEY(supplie_space_id, activity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE family_member (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, age INT NOT NULL, color VARCHAR(255) NOT NULL, type TINYINT(1) NOT NULL, available_time INT DEFAULT NULL, INDEX IDX_B9D4AD6DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, begin_at DATETIME DEFAULT NULL, end_at DATETIME DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(500) DEFAULT NULL, time INT NOT NULL, age_required INT NOT NULL, logo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_supplie_space (activity_id INT NOT NULL, supplie_space_id INT NOT NULL, INDEX IDX_3EF98E481C06096 (activity_id), INDEX IDX_3EF98E4A5C4A3D6 (supplie_space_id), PRIMARY KEY(activity_id, supplie_space_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_family_member (activity_id INT NOT NULL, family_member_id INT NOT NULL, INDEX IDX_B80664A981C06096 (activity_id), INDEX IDX_B80664A9BC594993 (family_member_id), PRIMARY KEY(activity_id, family_member_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplie_space ADD CONSTRAINT FK_23D5120A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE supplie_space_activity ADD CONSTRAINT FK_1DB55858A5C4A3D6 FOREIGN KEY (supplie_space_id) REFERENCES supplie_space (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplie_space_activity ADD CONSTRAINT FK_1DB5585881C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE family_member ADD CONSTRAINT FK_B9D4AD6DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE activity_supplie_space ADD CONSTRAINT FK_3EF98E481C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_supplie_space ADD CONSTRAINT FK_3EF98E4A5C4A3D6 FOREIGN KEY (supplie_space_id) REFERENCES supplie_space (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_family_member ADD CONSTRAINT FK_B80664A981C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_family_member ADD CONSTRAINT FK_B80664A9BC594993 FOREIGN KEY (family_member_id) REFERENCES family_member (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplie_space_activity DROP FOREIGN KEY FK_1DB55858A5C4A3D6');
        $this->addSql('ALTER TABLE activity_supplie_space DROP FOREIGN KEY FK_3EF98E4A5C4A3D6');
        $this->addSql('ALTER TABLE activity_family_member DROP FOREIGN KEY FK_B80664A9BC594993');
        $this->addSql('ALTER TABLE supplie_space DROP FOREIGN KEY FK_23D5120A76ED395');
        $this->addSql('ALTER TABLE family_member DROP FOREIGN KEY FK_B9D4AD6DA76ED395');
        $this->addSql('ALTER TABLE supplie_space_activity DROP FOREIGN KEY FK_1DB5585881C06096');
        $this->addSql('ALTER TABLE activity_supplie_space DROP FOREIGN KEY FK_3EF98E481C06096');
        $this->addSql('ALTER TABLE activity_family_member DROP FOREIGN KEY FK_B80664A981C06096');
        $this->addSql('DROP TABLE supplie_space');
        $this->addSql('DROP TABLE supplie_space_activity');
        $this->addSql('DROP TABLE family_member');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE activity_supplie_space');
        $this->addSql('DROP TABLE activity_family_member');
    }
}
