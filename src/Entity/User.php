<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FamilyMember", mappedBy="user")
     */
    private $familymember;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SupplieSpace", mappedBy="user")
     */
    private $supplies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    public function __construct()
    {
        $this->familymember = new ArrayCollection();
        $this->supplies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|FamilyMember[]
     */
    public function getFamilymember(): Collection
    {
        return $this->familymember;
    }

    public function addFamilymember(FamilyMember $familymember): self
    {
        if (!$this->familymember->contains($familymember)) {
            $this->familymember[] = $familymember;
            $familymember->setUser($this);
        }

        return $this;
    }

    public function removeFamilymember(FamilyMember $familymember): self
    {
        if ($this->familymember->contains($familymember)) {
            $this->familymember->removeElement($familymember);
            // set the owning side to null (unless already changed)
            if ($familymember->getUser() === $this) {
                $familymember->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SupplieSpace[]
     */
    public function getSupplies(): Collection
    {
        return $this->supplies;
    }

    public function addSupply(SupplieSpace $supply): self
    {
        if (!$this->supplies->contains($supply)) {
            $this->supplies[] = $supply;
            $supply->setUser($this);
        }

        return $this;
    }

    public function removeSupply(SupplieSpace $supply): self
    {
        if ($this->supplies->contains($supply)) {
            $this->supplies->removeElement($supply);
            // set the owning side to null (unless already changed)
            if ($supply->getUser() === $this) {
                $supply->setUser(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
