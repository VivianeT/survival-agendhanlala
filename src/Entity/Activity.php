<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActivityRepository")
 */
class Activity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="integer")
     */
    private $Time;

    /**
     * @ORM\Column(type="integer")
     */
    private $AgeRequired;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Logo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SupplieSpace", mappedBy="Supplie_Activity")
     */
    private $supplieSpaces;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SupplieSpace", inversedBy="activities")
     */
    private $Supplie_Activity;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FamilyMember", inversedBy="activities")
     */
    private $FamilyMember_Activity;

    public function __construct()
    {
        $this->supplieSpaces = new ArrayCollection();
        $this->Supplie_Activity = new ArrayCollection();
        $this->FamilyMember_Activity = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->Time;
    }

    public function setTime(int $Time): self
    {
        $this->Time = $Time;

        return $this;
    }

    public function getAgeRequired(): ?int
    {
        return $this->AgeRequired;
    }

    public function setAgeRequired(int $AgeRequired): self
    {
        $this->AgeRequired = $AgeRequired;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->Logo;
    }

    public function setLogo(?string $Logo): self
    {
        $this->Logo = $Logo;

        return $this;
    }

    /**
     * @return Collection|SupplieSpace[]
     */
    public function getSupplieSpaces(): Collection
    {
        return $this->supplieSpaces;
    }

    public function addSupplieSpace(SupplieSpace $supplieSpace): self
    {
        if (!$this->supplieSpaces->contains($supplieSpace)) {
            $this->supplieSpaces[] = $supplieSpace;
            $supplieSpace->addSupplieActivity($this);
        }

        return $this;
    }

    public function removeSupplieSpace(SupplieSpace $supplieSpace): self
    {
        if ($this->supplieSpaces->contains($supplieSpace)) {
            $this->supplieSpaces->removeElement($supplieSpace);
            $supplieSpace->removeSupplieActivity($this);
        }

        return $this;
    }

    /**
     * @return Collection|SupplieSpace[]
     */
    public function getSupplieActivity(): Collection
    {
        return $this->Supplie_Activity;
    }

    public function addSupplieActivity(SupplieSpace $supplieActivity): self
    {
        if (!$this->Supplie_Activity->contains($supplieActivity)) {
            $this->Supplie_Activity[] = $supplieActivity;
        }

        return $this;
    }

    public function removeSupplieActivity(SupplieSpace $supplieActivity): self
    {
        if ($this->Supplie_Activity->contains($supplieActivity)) {
            $this->Supplie_Activity->removeElement($supplieActivity);
        }

        return $this;
    }

    /**
     * @return Collection|FamilyMember[]
     */
    public function getFamilyMemberActivity(): Collection
    {
        return $this->FamilyMember_Activity;
    }

    public function addFamilyMemberActivity(FamilyMember $familyMemberActivity): self
    {
        if (!$this->FamilyMember_Activity->contains($familyMemberActivity)) {
            $this->FamilyMember_Activity[] = $familyMemberActivity;
        }

        return $this;
    }

    public function removeFamilyMemberActivity(FamilyMember $familyMemberActivity): self
    {
        if ($this->FamilyMember_Activity->contains($familyMemberActivity)) {
            $this->FamilyMember_Activity->removeElement($familyMemberActivity);
        }

        return $this;
    }
}
