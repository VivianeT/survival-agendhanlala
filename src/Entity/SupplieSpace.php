<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SupplieSpaceRepository")
 */
class SupplieSpace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantity;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Activity", inversedBy="supplieSpaces")
     */
    private $Supplie_Activity;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Activity", mappedBy="Supplie_Activity")
     */
    private $activities;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="supplies")
     */
    private $user;

    public function __construct()
    {
        $this->Supplie_Activity = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->Quantity;
    }

    public function setQuantity(int $Quantity): self
    {
        $this->Quantity = $Quantity;

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getSupplieActivity(): Collection
    {
        return $this->Supplie_Activity;
    }

    public function addSupplieActivity(Activity $supplieActivity): self
    {
        if (!$this->Supplie_Activity->contains($supplieActivity)) {
            $this->Supplie_Activity[] = $supplieActivity;
        }

        return $this;
    }

    public function removeSupplieActivity(Activity $supplieActivity): self
    {
        if ($this->Supplie_Activity->contains($supplieActivity)) {
            $this->Supplie_Activity->removeElement($supplieActivity);
        }

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->addSupplieActivity($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
            $activity->removeSupplieActivity($this);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
